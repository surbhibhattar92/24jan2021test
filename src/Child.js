import delete_icon from "./Assets/delete_icon.png";
import "./Child.scss";
import axios from "axios";
import React from "react";

export class Child extends React.Component {
  updateCats = (id) => {
    let filteredCats = [];
    let { cats } = this.props;
    filteredCats = cats.filter((each) => each._id !== id);
    console.log("filteredCats: ,cats: ", filteredCats, cats);
    this.props.setCats(filteredCats);
  };

  deleteCat = (catid) => {
    axios
      .delete(`http://localhost:8001/cats/${catid}`)
      .then((res) => {
        console.log(res.data);
        this.updateCats(catid);
      })
      .catch((err) => console.log(err));
  };

  render() {
    return (
      <div>
        <label>{this.props.data.text}</label>
        <br />
        <div className="images-container">
          <img src={this.props.data.img} />
          <img
            src={delete_icon}
            className="delete-icon"
            onClick={() => this.deleteCat(this.props.data._id)}
          />
        </div>
      </div>
    );
  }
}
