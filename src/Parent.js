import { Child } from "./Child";
import React from "react";
import axios from "axios";

export class Parent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cats: null,
    };
  }

  setCats = (cats) => {
    this.setState({
      cats: cats,
    });
  };

  componentWillMount() {
    axios
      .get("http://localhost:8001/cats")
      .then((res) => this.setCats(res.data))
      .catch((err) => console.log(err));
  }

  render() {
    return (
      <div>
        <h2>CAT MUSEUM</h2>
        {this.state.cats &&
          this.state.cats.map((each) => (
            <Child
              data={each}
              key={each._id}
              setCats={this.setCats}
              cats={this.state.cats}
            />
          ))}
      </div>
    );
  }
}
