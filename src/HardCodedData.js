export const data = [
    {
      text:"Abyssinians",
      img:"https://www.purina.com/sites/g/files/auxxlc196/files/styles/kraken_generic_max_width_240/public/Abyssinian_body_7.jpg?itok=E0O3mW9s"
    },
    {
      text:"American Bobtail",
      img:"https://www.purina.com/sites/g/files/auxxlc196/files/styles/kraken_generic_max_width_240/public/AmericanBobtail_body_6.jpg?itok=JYmdZhAt"
    },
    {
      text:"American Curl Cat Breed",
      img:"https://www.purina.com/sites/g/files/auxxlc196/files/styles/kraken_generic_max_width_240/public/AmericanCurlSHA_body_6.jpg?itok=1-yoW3el"
    },
    {
      text:"American Shorthair Cat",
      img:"https://www.purina.com/sites/g/files/auxxlc196/files/styles/kraken_generic_max_width_240/public/AmericanShorthair_body_6.jpg?itok=UKAO6ZyE"
    },
    {
      text:"American Wirehair Cat Breed",
      img:"https://www.purina.com/sites/g/files/auxxlc196/files/styles/kraken_generic_max_width_240/public/AmericanWirehair_body_6.jpg?itok=LgJxqcDn"
    }
  ]